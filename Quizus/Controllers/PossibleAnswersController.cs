﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Quizus.Models;

namespace Quizus.Controllers
{
    public class PossibleAnswersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: PossibleAnswers
        public ActionResult Index()
        {
            var possibleAnswers = db.PossibleAnswers.Include(p => p.Question);
            return View(possibleAnswers.ToList());
        }

        // GET: PossibleAnswers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PossibleAnswer possibleAnswer = db.PossibleAnswers.Find(id);
            if (possibleAnswer == null)
            {
                return HttpNotFound();
            }
            return View(possibleAnswer);
        }

        // GET: PossibleAnswers/Create
        public ActionResult Create()
        {
            ViewBag.QuestionId = new SelectList(db.Questions, "id", "Text");
            return View();
        }

        // POST: PossibleAnswers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,QuestionId,Answer,IsCorrect")] PossibleAnswer possibleAnswer)
        {
            if (ModelState.IsValid)
            {
                db.PossibleAnswers.Add(possibleAnswer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.QuestionId = new SelectList(db.Questions, "id", "Text", possibleAnswer.QuestionId);
            return View(possibleAnswer);
        }

        // GET: PossibleAnswers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PossibleAnswer possibleAnswer = db.PossibleAnswers.Find(id);
            if (possibleAnswer == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuestionId = new SelectList(db.Questions, "id", "Text", possibleAnswer.QuestionId);
            return View(possibleAnswer);
        }

        // POST: PossibleAnswers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,QuestionId,Answer,IsCorrect")] PossibleAnswer possibleAnswer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(possibleAnswer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.QuestionId = new SelectList(db.Questions, "id", "Text", possibleAnswer.QuestionId);
            return View(possibleAnswer);
        }

        // GET: PossibleAnswers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PossibleAnswer possibleAnswer = db.PossibleAnswers.Find(id);
            if (possibleAnswer == null)
            {
                return HttpNotFound();
            }
            return View(possibleAnswer);
        }

        // POST: PossibleAnswers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PossibleAnswer possibleAnswer = db.PossibleAnswers.Find(id);
            db.PossibleAnswers.Remove(possibleAnswer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
