﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Quizus.Models;

namespace Quizus.Controllers
{
    public class PossibleAnswersDataController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/PossibleAnswersData
        public IQueryable<PossibleAnswer> GetPossibleAnswers()
        {
            return db.PossibleAnswers;
        }

        // GET: api/PossibleAnswersData/5
        [ResponseType(typeof(PossibleAnswer))]
        public IHttpActionResult GetPossibleAnswer(int id)
        {
            PossibleAnswer possibleAnswer = db.PossibleAnswers.Find(id);
            if (possibleAnswer == null)
            {
                return NotFound();
            }

            return Ok(possibleAnswer);
        }

        //// PUT: api/PossibleAnswersData/5
        //[ResponseType(typeof(void))]
        //public IHttpActionResult PutPossibleAnswer(int id, PossibleAnswer possibleAnswer)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != possibleAnswer.id)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(possibleAnswer).State = EntityState.Modified;

        //    try
        //    {
        //        db.SaveChanges();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!PossibleAnswerExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}

        //// POST: api/PossibleAnswersData
        //[ResponseType(typeof(PossibleAnswer))]
        //public IHttpActionResult PostPossibleAnswer(PossibleAnswer possibleAnswer)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.PossibleAnswers.Add(possibleAnswer);
        //    db.SaveChanges();

        //    return CreatedAtRoute("DefaultApi", new { id = possibleAnswer.id }, possibleAnswer);
        //}

        //// DELETE: api/PossibleAnswersData/5
        //[ResponseType(typeof(PossibleAnswer))]
        //public IHttpActionResult DeletePossibleAnswer(int id)
        //{
        //    PossibleAnswer possibleAnswer = db.PossibleAnswers.Find(id);
        //    if (possibleAnswer == null)
        //    {
        //        return NotFound();
        //    }

        //    db.PossibleAnswers.Remove(possibleAnswer);
        //    db.SaveChanges();

        //    return Ok(possibleAnswer);
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PossibleAnswerExists(int id)
        {
            return db.PossibleAnswers.Count(e => e.id == id) > 0;
        }
    }
}