namespace Quizus.Migrations
{
    using Quizus.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Quizus.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Quizus.Models.ApplicationDbContext";

        }

        protected override void Seed(Quizus.Models.ApplicationDbContext context)
        {
            context.Events.AddOrUpdate(
                e => e.id,
               new Event { id = 1, EventToken = "DistanceTrivia", Text="Distances Trivia", ScheduledStartTime=DateTime.Now, State=0  },
               new Event { id = 2, EventToken = "PhysicsTrivia", Text = "Physics Trivia", ScheduledStartTime = DateTime.Now, State = 0 }
                );

            context.Questions.AddOrUpdate(
                q => q.id,
                new Question { id = 11, EventId = 1, Text = "How far is the Earth from the Sun", AnswerInfo="If the sun explodes it will take 8 minutes to see it!" },
                new Question { id = 12, EventId = 1, Text = "How far is the Moon from the Earth", AnswerInfo="That's why there was always a delay when Houston and Apollo 11 talked." },
                new Question { id = 13, EventId = 1, Text = "How far away is the star Proxima Centari (our closest celestial neighbor)" },
                new Question { id = 14, EventId = 1, Text = "How far away is the Andromeda Galaxy (which is closest to our Milky Way)", AnswerInfo="And it's headed right for us!  Look out it gonna crash!" },
                new Question { id = 15, EventId = 1, Text = "How far is the Earth from the Center of our Milky Way Galaxy?", AnswerInfo="And did you know the Center is a super massive black hole!" },
                new Question { id = 21, EventId = 2, Text = "Why are planets roughly spherical", AnswerInfo="Gravity is always the answer" },
                new Question { id = 22, EventId = 2, Text = "Why is the color white not part of a rainbow" },
                new Question { id = 23, EventId = 2, Text = "What element was discovered in the sun, before discovered on earth?", AnswerInfo="The spectral lines we saw in the Sun showed the existance of Helium." },
                new Question { id = 24, EventId = 2, Text = "What effect makes fussion in the sun possible?", AnswerInfo= "Without quantum tunneling, even 27 million Fahenheit isn't warm enough." }
                );

            context.PossibleAnswers.AddOrUpdate(
                pa => pa.id,
                new PossibleAnswer { id = 111, QuestionId = 11, IsCorrect=false, Answer = "8 light seconds" },
                new PossibleAnswer { id = 112, QuestionId = 11, IsCorrect=true,  Answer = "8 light minutes" },
                new PossibleAnswer { id = 113, QuestionId = 11, IsCorrect=false, Answer = "8 light hours" },
                new PossibleAnswer { id = 114, QuestionId = 11, IsCorrect=false, Answer = "8 light years" },
                new PossibleAnswer { id = 121, QuestionId = 12, IsCorrect=true,  Answer = "1.3 light seconds" },
                new PossibleAnswer { id = 122, QuestionId = 12, IsCorrect=false, Answer = "1.3 light minutes" },
                new PossibleAnswer { id = 123, QuestionId = 12, IsCorrect=false, Answer = "1.3 light hours" },
                new PossibleAnswer { id = 124, QuestionId = 12, IsCorrect=false, Answer = "1.3 light years" },
                new PossibleAnswer { id = 131, QuestionId = 13, IsCorrect=false, Answer = "4 light seconds" },
                new PossibleAnswer { id = 132, QuestionId = 13, IsCorrect=false, Answer = "4 light minutes" },
                new PossibleAnswer { id = 133, QuestionId = 13, IsCorrect=false, Answer = "4 light hours" },
                new PossibleAnswer { id = 134, QuestionId = 13, IsCorrect=true,  Answer = "4 light years" },
                new PossibleAnswer { id = 141, QuestionId = 14, IsCorrect = false, Answer = "2.5 light years" },
                new PossibleAnswer { id = 142, QuestionId = 14, IsCorrect = false, Answer = "2.5 thousand light years" },
                new PossibleAnswer { id = 143, QuestionId = 14, IsCorrect = true, Answer = "2.5 million light years" },
                new PossibleAnswer { id = 144, QuestionId = 14, IsCorrect = false, Answer = "2.5 billion light years" },
                new PossibleAnswer { id = 151, QuestionId = 15, IsCorrect = false, Answer = "100 light years" },
                new PossibleAnswer { id = 152, QuestionId = 15, IsCorrect = true, Answer = "100 thousand light years" },
                new PossibleAnswer { id = 153, QuestionId = 15, IsCorrect = false, Answer = "100 million light years" },
                new PossibleAnswer { id = 154, QuestionId = 15, IsCorrect = false, Answer = "100 billion light years" },
                new PossibleAnswer { id = 211, QuestionId = 21, IsCorrect = false, Answer = "Meteors bombard them causing there shape to be roughly round" },
                new PossibleAnswer { id = 212, QuestionId = 21, IsCorrect = false, Answer = "Lower gravity at the farthest points cause those peices to fall off" },
                new PossibleAnswer { id = 213, QuestionId = 21, IsCorrect = false, Answer = "Centrifical forces cause fatherest point to fly away" },
                new PossibleAnswer { id = 214, QuestionId = 21, IsCorrect = true, Answer = "Beyond a certain mass, gravity causes a spherical shape" },
                new PossibleAnswer { id = 221, QuestionId = 22, IsCorrect = true, Answer = "White is not a single color, it's a combination of all the colors" },
                new PossibleAnswer { id = 222, QuestionId = 22, IsCorrect = false, Answer = "The white portion gets red-shifted due to light speed" },
                new PossibleAnswer { id = 223, QuestionId = 22, IsCorrect = false, Answer = "White is the first color of the rainbow" },
                new PossibleAnswer { id = 224, QuestionId = 22, IsCorrect = false, Answer = "The white is just too faint to see" },
                new PossibleAnswer { id = 231, QuestionId = 23, IsCorrect = false, Answer = "Hydrogen" },
                new PossibleAnswer { id = 232, QuestionId = 23, IsCorrect = true, Answer = "Helium" },
                new PossibleAnswer { id = 233, QuestionId = 23, IsCorrect = false, Answer = "Lithium" },
                new PossibleAnswer { id = 234, QuestionId = 23, IsCorrect = false, Answer = "Oxygen" },
                new PossibleAnswer { id = 241, QuestionId = 24, IsCorrect = true, Answer = "Quantum tunnelling" },
                new PossibleAnswer { id = 242, QuestionId = 24, IsCorrect = false, Answer = "Covarient bonds forming" },
                new PossibleAnswer { id = 243, QuestionId = 24, IsCorrect = false, Answer = "The temperature (27 million Fahenheit)" },
                new PossibleAnswer { id = 244, QuestionId = 24, IsCorrect = false, Answer = "Ionic bonds collapsing" }
                );
        }
    }
}
