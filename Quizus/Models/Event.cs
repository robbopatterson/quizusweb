﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Quizus.Models
{
    public class Event
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Event Identifier Token", AutoGenerateFilter = true)]
        public String EventToken { get; set; }

        [Display(Name = "Event Description", AutoGenerateFilter = true)]
        public String Text { get; set; }

        public DateTime ScheduledStartTime { get; set; }

        public int State { get; set; } // 0 - not started, 1 - running, 2-ended

        // Todo: Category
        // Todo: Theme

        [JsonIgnore]
        public virtual ICollection<Question> Questions { get; set; }
    }
}