﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Quizus.Models
{
    public class PossibleAnswer
    {
        [Key]
        public int id { get; set; }

        public int QuestionId { get; set; }
        [ForeignKey("QuestionId")]
        [JsonIgnore]
        public virtual Question Question { get; set; }

        [Display(Name = "Answer", AutoGenerateFilter = false)]
        public string Answer { get; set; }

        [Required]
        public bool IsCorrect { get; set; }
    }
}