﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Quizus.Models
{
    public class Question
    {
        [Key]
        public int id { get; set; }

        public int EventId { get; set; }
        [ForeignKey("EventId")]
        [JsonIgnore]
        public virtual Event Event{ get; set; }

        [Required]
        [Display(Name = "Question", AutoGenerateFilter = false)]
        public string Text { get; set; }

        [Display(Name = "AnswerInfo", AutoGenerateFilter = false)]
        public string AnswerInfo { get; set; }

        [Display(Name = "Duration (in seconds)", AutoGenerateFilter = false)]
        public int Duration { get; set; }
    }
}