﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Quizus.Startup))]
namespace Quizus
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
